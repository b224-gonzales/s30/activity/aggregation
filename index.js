// Aggregation


// Count Operator

db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: "$onSale", fruitsOnSale: {$sum: "$stock"} } }

	]);


// Stock more than 20

db.fruits.aggregate([
		{ $match: {"stock": {$gt: 20}}},
		{ $group: {_id: "stock", enoughStock: {$sum: "$stock"} } }

	]);


// Average Price of fruits

db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: "$supplier_id", avg_price: {$avg: "$price"} } }

	]);


// Max Price

db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: "$supplier_id", max_price: {$max: "$price"} } }

	]);


// Min Price

db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: "$supplier_id", min_price: {$min: "$price"} } }

	]);